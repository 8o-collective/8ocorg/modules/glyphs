import styled from "styled-components";

const GlyphContainer = styled.div``;

const GlyphCanvas = styled.canvas`
  position: absolute;
  top: 50vh;
  left: 75vw;
  width: 25vw;
  height: 50vh;
  transform: translate(-50%, -50%);

  margin: 0px 3vw;
  border: 1px solid #222;
  image-rendering: pixelated;
`;

const GlyphInput = styled.input`
  position: absolute;
  top: 50vh;
  left: 25vw;
  width: 25vw;
  transform: translate(-50%, -50%);

  font-family: "IBM3270";

  color: red;
  background-color: inherit;
  border-color: red;
`;

export { GlyphContainer, GlyphCanvas, GlyphInput };
