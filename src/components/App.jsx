import React from "react";

import { AppContainer } from "assets/styles/App.styles.jsx";

import Glyph from "components/Glyph.jsx";

const App = () => {
  return (
    <AppContainer>
      <Glyph />
    </AppContainer>
  );
};

export default App;
