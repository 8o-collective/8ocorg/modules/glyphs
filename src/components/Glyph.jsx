import React from "react";

import {
  GlyphContainer,
  GlyphCanvas,
  GlyphInput,
} from "assets/styles/Glyph.styles.jsx";

const Glyph = () => {
  return (
    <GlyphContainer>
      <GlyphCanvas />
      <GlyphInput />
    </GlyphContainer>
  );
};

export default Glyph;
